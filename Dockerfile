#
# Compile
#

FROM golang:1.13-alpine as build

ARG VERSION

ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    VERSION=${VERSION}

# hadolint ignore=DL3018
RUN apk add --no-cache curl ca-certificates git

# build intel-device-plugins-for-kubernetes
WORKDIR /go/src/github.com/intel/intel-device-plugins-for-kubernetes

RUN git clone --depth 1 -b ${VERSION} https://github.com/intel/intel-device-plugins-for-kubernetes.git .
RUN go build -o intel_gpu_device_plugin ./cmd/gpu_plugin

#
# Deploy
#

FROM alpine:3.11

# hadolint ignore=DL3018
RUN apk add --no-cache ca-certificates tini

# hadolint ignore=DL3022
COPY --from=build /go/src/github.com/intel/intel-device-plugins-for-kubernetes/intel_gpu_device_plugin /usr/local/bin/intel_gpu_device_plugin
RUN chmod +x /usr/local/bin/intel_gpu_device_plugin

ENTRYPOINT ["tini", "--"]
CMD ["/usr/local/bin/intel_gpu_device_plugin"]
